
#include<iostream>
#include <windows.h>
#include<process.h>
using namespace std;

long g_nNum;
unsigned int _stdcall Fun(void * pPM);

const int THREAD_NUM = 10;
HANDLE g_hThreadParameter; //信号量
CRITICAL_SECTION g_csThreadCode; //临界区

int main()
{	
	g_hThreadParameter = CreateSemaphore(NULL, 0, 1, NULL);   //四个参数依次为：安全属性、 信号量的初始值、信号量的最大值、信号量名
	InitializeCriticalSection(&g_csThreadCode);

	HANDLE handle[THREAD_NUM];
	int i = 0;
	while (i < THREAD_NUM) {
		handle[i] = (HANDLE)_beginthreadex(NULL, 0, Fun, &i, 0, NULL);
		Sleep(1);
		WaitForSingleObject(&g_hThreadParameter, INFINITE);   //mutex的release就是把资源从本线程释放出去，再让其他线程去争抢，releasesemaphore则是通知waitforxxobject的线程可以运行了
		++i;
	}

	WaitForMultipleObjects(THREAD_NUM, handle, TRUE, INFINITE);

	DeleteCriticalSection(&g_csThreadCode);
	CloseHandle(g_hThreadParameter);
	
	for (i = 0; i < THREAD_NUM; i++)
		CloseHandle(handle[i]);
	
	return 0;
}


unsigned int _stdcall Fun(void *pPM) {
	int nThreadNum = *(int*)pPM;
	ReleaseSemaphore(g_hThreadParameter, 1, NULL); //信号量++ , 当前资源数量大于0，表示信号量处于触发，等于0表示资源已经耗尽故信号量处于末触发。
	Sleep(50);
	EnterCriticalSection(&g_csThreadCode);
	++g_nNum;
	Sleep(10);//do something
	printf("线程编号为%d 全局资源值为%d\n", nThreadNum, g_nNum);
	LeaveCriticalSection(&g_csThreadCode);
	return 0;
}
