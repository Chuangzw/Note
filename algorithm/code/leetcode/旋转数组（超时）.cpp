#include <iostream>
#include<vector>
using namespace std;

void swap(vector<int> &nums)
{
    int t = nums.back();
    for (int i = nums.capacity() - 2; i >= 0; i--)
    {
        nums[i + 1] = nums[i];
    }
    nums[0] = t;
}
void rotate(vector<int> &nums, int k)
{

    while (k--)
    {
        swap(nums);
    }

    vector<int>::iterator it; //声明一个迭代器，来访问vector容器，作用：遍历或者指向vector容器的元素
    for (it = nums.begin(); it != nums.end(); it++)
    {
        cout << *it << " ";
    }
}

int main()
{
    vector<int> nums = {1, 2, 3, 4, 5, 6, 7};
    int k = 3;
    rotate(nums, k);
  
    return 0;
}
