#include<iostream>
#include "stdio.h"
#include<vector>
using namespace std;
int removeDuplicates(vector<int>& nums) {
        if (nums.empty()) {
            return 0;
        }   
        int number = 0;
        for (int i = 0; i < nums.size(); i++) {
            if (nums[number] != nums[i]) {
                number ++;
                nums[number] = nums[i];
            }
        }
        return (number + 1);
    }
int main(){
    vector<int> nums = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
    // nums 是以“引用”方式传递的。也就是说，不对实参做任何拷贝
    int len = removeDuplicates(nums);
    // 在函数里修改输入数组对于调用者是可见的。
    // 根据你的函数返回的长度, 它会打印出数组中该长度范围内的所有元素。
    for (int i = 0; i < len; i++) {
        printf("%d",nums[i]);
    }
    return 0;
}
